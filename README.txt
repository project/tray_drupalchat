/* $Id$ */

-- SUMMARY --

This module integrates drupalchat into the Tray


-- REQUIREMENTS --

Tray (http://drupal.org/project/tray)
DrupalChat (http://drupal.org/project/drupalchat)


-- INSTALLATION --

Enable the module. 

